﻿angular.module('ui.bootstrap.demo', ['ngAnimate', 'ui.bootstrap']);
angular.module('ui.bootstrap.demo').controller('ModalDemoCtrl', function ($scope, $uibModal, $log) {

    $scope.items = ['item1', 'item2', 'item3'];
    $scope.newField;

    $scope.animationsEnabled = true;

    $scope.open = function (size) {

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl',
            size: size,
            resolve: {
                newField: function(){
                    return $scope.newField;
                },
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem, field) {
            //$scope.selected = selectedItem;
            //$scope.field = field;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.toggleAnimation = function () {
        $scope.animationsEnabled = !$scope.animationsEnabled;
    };

});


angular.module('ui.bootstrap.demo').controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, items, newField) {

    $scope.newField = newField;

    $scope.items = items;
    $scope.selected = {
        item: $scope.items[0]
    };

    $scope.ok = function () {
        $uibModalInstance.close($scope.selected.item, $scope.newField);
        if ($scope.newField && (('name' in $scope.newField) && ('type' in $scope.newField) && ('value' in $scope.newField))) {
            switch ($scope.newField.type) {
                case 'text': var type = '<img src="/Content/Text Color-48.png">'; break;
                case 'dateTime': var type = '<img src="/Content/Calendar-48.png">'; break;
                case 'boolean': var type = '<img src="/Content/Checked Checkbox 2-64.png">'; break;
                case 'reference': var type = '<img src="/Content/Link Filled-50.png">'; break;
                case 'string': var type = '<img src="/Content/Text Color-48.png">'; break;
            }
            var checked = $('#multi').prop('checked');

            var multival = checked ? '<img src="/Content/List-48.png">' : '';
            var str = $scope.newField.type == 'string' ? '<img src="/Content/Tree Structure-64.png">' : '';
            var tempTemplate =
            '<div class="row">' +
                '<div class="col-xs-7"><span>' + $scope.newField.name + '</span></div><div class="col-xs-3"><span>' + type + '</span>&nbsp;&nbsp;' + str + '&nbsp;&nbsp;' + multival + '</div><div class="col-xs-2"><button class="btn btn-xs edit"><i class="glyphicon glyphicon-pencil"></i></button><button class="btn btn-xs remove"><i class="glyphicon glyphicon-remove"></i></button></div>' +
            '</div>';

            var addHeight = actualHeight + 21;
            $('.content a').before(tempTemplate);
            el1.resize(actualWidth, addHeight);
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
